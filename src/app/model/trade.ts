enum TradeType{
  BUY = 'BUY',
  SELL = 'SELL'
}

enum TradeState{
  CREATED = 'CREATED',
  PROCESSING = 'PROCESSING',
  FILLED = 'FILLED',
  REJECTED = 'REJECTED'
}

export class Trade{

  public state: TradeState;
  public date: Date;
  public totalPrice: number;
  public unitPrice: number;
  public userId: string;
  public ticker: string;
  public type: TradeType;
  public quantity: number;

  constructor(ticker: string, quantity: string, type: string){
    this.ticker = ticker;
    this.quantity = Number(quantity);
    this.type = type as TradeType;
  }
}


