import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { HoldingService } from '../holdings-service/holding.service';
import { Observable, BehaviorSubject, fromEvent, pipe } from 'rxjs';
import { debounceTime, delay, distinctUntilChanged, map, tap } from 'rxjs/operators';
import { Holding } from '../holdings-service/holding';
import { MatPaginator } from '@angular/material/paginator';
import { MatSpinner } from '@angular/material/progress-spinner'
import { MatInput } from '@angular/material/input'
import { FetcherService } from '../price-fetcher/fetcher.service';
import { Quote } from '../price-fetcher/quote';
import { MatSort } from '@angular/material/sort';

@Component({
  selector: 'app-holding',
  templateUrl: './holding.component.html',
  styleUrls: ['./holding.component.css']
})
export class HoldingComponent implements OnInit {

  public holdings$: Observable<Array<Holding>>;
  displayedColumns: string[] = ['ticker', 'companyName', 'bought', 'sold', 'balance', 'price'];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private holdingService : HoldingService, private fetcher : FetcherService) {
  }

  getPrice(ticker : string) : Observable<Quote> {
    return this.fetcher.fetch(ticker);
  }

  public ngOnInit() {
    this.holdings$ = this.holdingService.getHoldings();
    console.log(this.holdings$);
  }

}
