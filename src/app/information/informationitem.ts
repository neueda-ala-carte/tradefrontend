export class InformationItem {
    
    constructor(public ticker: string, public date: string, public name: string, public currentPrice: string ) {}

  }